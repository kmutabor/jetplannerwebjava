package app.mbean;

import java.util.List;

public interface EventServiceMXBean {
    public List<String> getExpiredEventList();
    public int getExpiredCount();
    public void removeExpired();

}
