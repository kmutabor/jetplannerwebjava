package app.util;

import app.model.Address;
import app.model.Event;
import app.model.Job;
import app.model.User;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/*

 */
public class ResponseHelper {


    private final static Event event = new Event(0, 0, 0, "error", "error");
    private final static Address address = new Address(0, "error", 0.0,0.0);
    private final static Job job = new Job(0, 0, 0, "error", "error");
    private final static User user = new User(0, "error", "error", "error");

    static {
        job.setUser(user);
        job.setAddress(address);
    }

    private ResponseHelper() throws ParseException {}

    public static User getEmptyUser() {
        return user;
    }

    public static List<User> getEmptyUserList() {
        List empty = new ArrayList();
        empty.add(user);
        return empty;
    }

    public static List<Event> getEmptyEventList() {
        List empty = new ArrayList();
        empty.add(event);
        return empty;
    }

    public static Event getEmptyEvent() {
        return event;
    }


    public static List<Job> getEmptyJobList() {
        List empty = new ArrayList();
        empty.add(job);
        return empty;
    }

    public static Job getEmptyJob() {
        return job;
    }

//    public static List<Address> getEmptyAddressList() {
//        List empty = new ArrayList();
//        empty.add(address);
//        return empty;
//    }

    public static Address getEmptyAddress() {
        return address;
    }
}
