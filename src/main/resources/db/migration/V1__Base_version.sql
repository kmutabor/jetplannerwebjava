create table address
(
  id int auto_increment
    primary key,
  address varchar(255) default 'not set' not null,
  latitude decimal(10,6) default '0.000000' not null,
  longitude decimal(10,6) default '0.000000' not null
)
;

create table event
(
  id int auto_increment
    primary key,
  user_id int not null,
  job_id int not null,
  status enum('approved', 'cancelled', 'denied', 'done', 'request', 'expired') not null,
  desired_time datetime not null
)
;

create index event_job_id_fk
  on event (job_id)
;

create index event_user_id_fk
  on event (user_id)
;

create table job
(
  id int auto_increment
    primary key,
  user_id int not null,
  address_id int not null,
  name varchar(45) not null,
  amount int default '100' not null,
  description text null,
  time_interval int default '60' not null,
  work_hour_start int default '8' not null,
  work_hour_end int default '16' not null
)
;

create index job_address_id_fk
  on job (address_id)
;

create index job_user_id_fk
  on job (user_id)
;

alter table event
  add constraint event_job_id_fk
foreign key (job_id) references job (id)
  on delete cascade
;

create table log
(
  id int auto_increment
    primary key,
  type enum('registration', 'create', 'update', 'delete') not null,
  user_id int not null,
  user_phone varchar(15) null,
  user_name varchar(30) null,
  job_id int null,
  event_id int null,
  address_id int null,
  description text not null,
  create_time datetime default CURRENT_TIMESTAMP not null,
  constraint log_id_uindex
  unique (id)
)
;

create table user
(
  id int auto_increment
    primary key,
  phone varchar(15) not null,
  token varchar(255) not null,
  name varchar(30) not null,
  constraint user_phone_uindex
  unique (phone),
  constraint user_token_uindex
  unique (token)
)
;

alter table event
  add constraint event_user_id_fk
foreign key (user_id) references user (id)
  on delete cascade
;

alter table job
  add constraint job_user_id_fk
foreign key (user_id) references user (id)
  on delete cascade
;

