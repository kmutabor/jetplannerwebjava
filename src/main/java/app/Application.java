package app;

import app.dao.EventDAO;
import app.dao.impl.EventDAOImpl;
import app.mbean.impl.EventService;
import app.shedule.EventChangeStatusJob;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.management.*;
import java.lang.management.ManagementFactory;

@ComponentScan
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        ObjectName mxbeanName = null;
        try {

            mxbeanName = new ObjectName("jetPlannerRestService:type=EventService,name=EventExpiredService");
            ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
            EventDAO eventDAO = (EventDAOImpl) context.getBean("eventDAO");
            EventService eventServiceMBean = new EventService(eventDAO);
            mbs.registerMBean(eventServiceMBean, mxbeanName);

        } catch (MalformedObjectNameException e) {
            e.printStackTrace();
        } catch (NotCompliantMBeanException e) {
            e.printStackTrace();
        } catch (InstanceAlreadyExistsException e) {
            e.printStackTrace();
        } catch (MBeanRegistrationException e) {
            e.printStackTrace();
        }


        JobDetail eventChangeStatusJob = JobBuilder.newJob(EventChangeStatusJob.class)
                .withIdentity("changeEventStatusJobName", "event1").build();

        Trigger trigger = TriggerBuilder
                .newTrigger()
                .withIdentity("changeEventStatusTriggerName", "event1")
                .withSchedule(CronScheduleBuilder.cronSchedule("0 0 0/1 * * ?"))
                .build();


        Scheduler scheduler = null;
        try {
            scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
            scheduler.scheduleJob(eventChangeStatusJob, trigger);

        } catch (SchedulerException e) {
            e.printStackTrace();
        }

    }
}