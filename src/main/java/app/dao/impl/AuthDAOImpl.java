package app.dao.impl;

import app.dao.AuthDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthDAOImpl implements AuthDAO {

    private final static Log LOGGER = LogFactory.getLog(AuthDAOImpl.class);

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public boolean auth(int userId, String token) {
        String sql = "SELECT id FROM user WHERE id = ? AND token = ?";
        Connection conn = null;
        boolean isRegistered = false;
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, userId);
            ps.setString(2, token);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                isRegistered = true;
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
            isRegistered = false;
            LOGGER.error(e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    isRegistered = false;
                    LOGGER.error(e.getMessage());
                }
            }
        }
        return isRegistered;
    }
}
