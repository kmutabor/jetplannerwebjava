package app.model;

public class User {

    private int id;
    private String phone;
    private String token;
    private String name;


    public User(String token, String phone, String name) {
        this.token = token;
        this.phone = phone;
        this.name = name;
    }

    public User(int id, String token, String phone, String name) {
        this.id = id;
        this.token = token;
        this.phone = phone;
        this.name = name;

    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "[" +
                "id="+getId() + ", " +
                "name='"+getName() + "', " +
                "phone='"+getPhone() + "', " +
                "token='"+getToken()+"'"+
                "]";
    }

}
