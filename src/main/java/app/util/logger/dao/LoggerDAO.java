package app.util.logger.dao;

import app.util.logger.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class LoggerDAO {

    private final static Log LOGGER = LogFactory.getLog(LoggerDAO.class);

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }




    public void write(String type, int user_id, String user_phone, String user_name,
                      int job_id, int event_id, int address_id, String description) {
        String sqlInsert = "INSERT INTO log (" +
                "type, " +
                "user_id, " +
                "user_phone, " +
                "user_name, " +
                "job_id, " +
                "event_id, " +
                "address_id, " +
                "description, " +
                "create_time " +
                ") " +
                "VALUES (?,?,?,?,?,?,?,?,NOW())";

//        LOGGER.error(""+type+" "+ user_id+" "+user_phone+" "+user_name+" "+ job_id+" "+ event_id+" "+ address_id+" "+description);
        Connection conn = null;
        PreparedStatement ps = null;
        try {

            conn = dataSource.getConnection();

            ps = conn.prepareStatement(sqlInsert);

            ps.setString(1, type);
            ps.setInt(2, user_id);
            ps.setString(3, user_phone);
            ps.setString(4, user_name);
            ps.setInt(5, job_id);
            ps.setInt(6, event_id);
            ps.setInt(7, address_id);
            ps.setString(8, description);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
