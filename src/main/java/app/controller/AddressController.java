package app.controller;

import app.dao.AddressDAO;
import app.dao.impl.AddressDAOImpl;
import app.model.Address;
import app.util.AuthHelper;
import app.util.Constants;
import app.util.ResponseHelper;
import app.util.logger.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.*;


@RestController
public class AddressController {

    private final static Log LOGGER = LogFactory.getLog(AddressController.class);

    private Logger logger = new Logger();

    private ApplicationContext context;
    private AddressDAO addressDAO;
    private AuthHelper authHelper;

    AddressController(){
        context = new ClassPathXmlApplicationContext("Spring-Module.xml");
        addressDAO = (AddressDAOImpl) context.getBean("addressDAO");
        authHelper = new AuthHelper();
    }

    @RequestMapping(value = Constants.Request.ADDRESS, method = RequestMethod.POST)
    public Address create(
            @RequestParam(value = "user_id", required = true) int userId,
            @RequestParam(value = "token", required = true) String token,
            @RequestParam(value = "address", required = true) String address,
            @RequestParam(value = "latitude", required = true) double latitude,
            @RequestParam(value = "longitude", required = true) double longitude
    ) {

        if (!authHelper.checkAuth(userId, token)) {
            return ResponseHelper.getEmptyAddress();
        }

        Address addressModel = new Address(address, latitude, longitude);

        int addressId = addressDAO.insert(addressModel);

        addressModel.setId(addressId);

        logger.write(Logger.TYPE_CREATE, userId, null, null, 0,0,addressModel.getId(), "Добавлен address '"+addressModel.getAddress()+"' "+addressModel.toString());


        return addressModel;

    }


    @RequestMapping(value = Constants.Request.ADDRESS, method = RequestMethod.PUT,params = {"user_id", "token", "address_id", "address", "latitude", "longitude"})
    public Address update(
            @RequestParam(value = "user_id", required = true) int userId,
            @RequestParam(value = "token", required = true) String token,
            @RequestParam(value = "address_id", required = true) int addressId,
            @RequestParam(value = "address", required = true) String address,
            @RequestParam(value = "latitude", required = true) double latitude,
            @RequestParam(value = "longitude", required = true) double longitude
    ){
        if (!authHelper.checkAuth(userId, token)) {
            return ResponseHelper.getEmptyAddress();
        }

        Address addressModel = addressDAO.findById(addressId);
        addressModel.setId(addressId);
        addressModel.setAddress(address);
        addressModel.setLatitude(latitude);
        addressModel.setLongitude(longitude);
        int updated = addressDAO.update(addressModel);
        if(1 == updated){
            LOGGER.info("Update address to: "+addressModel);
            logger.write(Logger.TYPE_UPDATE, userId, "", "", addressModel.getId(),0,addressModel.getId(), "Изменен address '"+addressModel.getAddress()+ "' "+addressModel.toString());
            return addressModel;
        }
        LOGGER.error("Updated address was rejected! Saved old data.");
        return ResponseHelper.getEmptyAddress();
    }

    @RequestMapping(value = Constants.Request.ADDRESS, method = RequestMethod.GET, params = {"user_id", "token", "address_id"})
    public Address getAddressById(
            @RequestParam(value = "user_id", required = true) int userId,
            @RequestParam(value = "token", required = true) String token,
            @RequestParam(value = "address_id", required = true) int addressId) {
        if (!authHelper.checkAuth(userId, token)) {
            return ResponseHelper.getEmptyAddress();
        }

        return addressDAO.findById(addressId);
    }


    @RequestMapping(value = Constants.Request.ADDRESS + "/{address_id}/{user_id}/{token}", method = RequestMethod.DELETE)
    public Address delete(
            @PathVariable(value = "address_id", required = true) int addressId,
            @PathVariable(value = "user_id", required = true) int userId,
            @PathVariable(value = "token", required = true) String token
    ) {

        if (!authHelper.checkAuth(userId, token)) {
            return ResponseHelper.getEmptyAddress();
        }

        Address address = addressDAO.findById(addressId);
        if(address.getId() > 0){
            logger.write(Logger.TYPE_DELETE, userId, null, null, 0,0, address.getId(), "Удален address '"+address.getAddress()+"' "+address.toString());
        }

        return addressDAO.delete(addressId, userId, token);

    }


}
