package app.dao;


import app.model.Job;

import java.util.List;

public interface JobDAO {
    public int insert(Job job);

    public List<Job> findByUserId(int userId);

    public List<Job> findByName(String name);

    public List<Job> findByDescription(String description);

    public Job findById(int id);

    public int update(String token, Job job);

    public Job delete(int jobId, int userId, String token);
}
