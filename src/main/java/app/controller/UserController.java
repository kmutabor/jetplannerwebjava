package app.controller;

import app.dao.UserDAO;
import app.dao.impl.UserDAOImpl;
import app.model.User;
import app.util.AuthHelper;
import app.util.Constants;
import app.util.ResponseHelper;
import app.util.logger.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.*;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

@RestController
public class UserController {

    private final static Log LOGGER = LogFactory.getLog(UserController.class);

    private Logger logger = new Logger();

    private ApplicationContext context;
    private User user;
    private UserDAO userDAO;
    private AuthHelper authHelper;

    UserController(){
        context = new ClassPathXmlApplicationContext("Spring-Module.xml");
        userDAO = (UserDAOImpl) context.getBean("userDAO");
        authHelper = new AuthHelper();
    }

    @RequestMapping(value = Constants.Request.USER, method = RequestMethod.POST)
    public User registration(@RequestParam(value = "phone", required = true) String phone,
                             @RequestParam(value = "name", required = true) String name) {

        user = userDAO.findByUserPhone(phone);

        if (0 < user.getId()) {
            return ResponseHelper.getEmptyUser();
        }

        MessageDigest md = null;
        try {
            String origin = phone + new Date().toString();
            md = MessageDigest.getInstance("MD5");
            md.update(origin.getBytes());
            byte[] digest = md.digest();
            StringBuffer sb = new StringBuffer();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }
            String token = sb.toString();
            user = new User(token, phone, name);

            int userId = userDAO.insert(user);

            user.setId(userId);

        } catch (NoSuchAlgorithmException e) {
            user = ResponseHelper.getEmptyUser();
        }

        logger.write(Logger.TYPE_REGISTRATION, user.getId(), user.getPhone(), user.getName(), 0,0,0, "Регистрация пользователя '"+user.getName()+"' "+ user.toString());

        return user;

    }



    @RequestMapping(value = Constants.Request.USER + "/{user_id}/{token}", method = RequestMethod.DELETE)
    public User delete(@PathVariable(value = "user_id", required = true) int userId, @PathVariable(value = "token", required = true) String token) {
        if (!authHelper.checkAuth(userId, token)) {
            return ResponseHelper.getEmptyUser();
        }
        user = userDAO.findById(userId);

        if(0 < user.getId()){
            logger.write(Logger.TYPE_DELETE, userId, user.getPhone(), user.getName(), 0,0,0, "Удаление аккаунта пользователя '"+user.getName()+ "' "+ user.toString());
        }
        return userDAO.delete(userId, token);
    }
}
