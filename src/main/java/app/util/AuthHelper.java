package app.util;

import app.dao.AuthDAO;
import app.dao.impl.AuthDAOImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AuthHelper {

    private AuthDAO authDAO;
    private ApplicationContext context = null;

    public AuthHelper() {
        context = new ClassPathXmlApplicationContext("Spring-Module.xml");
        authDAO = (AuthDAOImpl) context.getBean("authDAO");
        ;
    }

    public boolean checkAuth(int userId, String token) {
        return this.authDAO.auth(userId, token);
    }
}
