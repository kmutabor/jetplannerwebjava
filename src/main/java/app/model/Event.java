package app.model;

public class Event {

    private int id;
    private int userId;
    private int jobId;
    private String status;
    private String desiredTime;

    private Job job;
    private User user;

    public Event(int userId, int jobId, String status, String desiredTime) {
        this.userId = userId;
        this.jobId = jobId;
        this.status = status;
        this.desiredTime = desiredTime;
    }

    public Event(int id, int userId, int jobId, String status, String desiredTime) {
        this.id = id;
        this.userId = userId;
        this.jobId = jobId;
        this.status = status;
        this.desiredTime = desiredTime;
    }


    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getJobId() {
        return this.jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesiredTime() {
        return this.desiredTime;
    }

    public void setDesiredTime(String desiredTime) {
        this.desiredTime = desiredTime;
    }

    public Job getJob() {
        return this.job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String toString() {
        return "["+
                "id="+getId() + ", " +
                "user_id="+getUserId() + ", " +
                "job_id="+getJobId() + ", " +
                "status='"+getStatus() + "', " +
                "desired_time='"+getDesiredTime() + "', " +
                "user="+getUser() + ", " +
                "job="+getJob()+
                "]";
    }
}
