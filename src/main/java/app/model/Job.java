package app.model;

import jdk.nashorn.internal.scripts.JO;

public class Job {

    private int id;
    private int userId;
    private int addressId;
    private String name;
    private int amount;
    private String description;
    private int timeInterval = 60; // по умолчанию 1 час
    private User user = null;
    private Address address = null;
    private int eventCount = 0;
    private int workHourStart = 8;
    private int workHourEnd = 16;


    public Job(int userId, int addressId, String name, String description) {
        this.userId = userId;
        this.addressId = addressId;
        this.name = name;
        this.description = description;
    }

    public Job(int userId, int addressId, String name, String description, int timeInterval) {
        this(userId, addressId, name, description);
        this.timeInterval = timeInterval;
    }

    public Job(int userId, int addressId, String name, String description, int timeInterval, int workHourStart, int workHourEnd) {
        this(userId, addressId, name, description, timeInterval);
        this.workHourStart = workHourStart;
        this.workHourEnd = workHourEnd;
    }

    public Job(int userId, int addressId, String name, String description, int timeInterval, int workHourStart, int workHourEnd, int eventCount) {
        this(userId, addressId, name, description, timeInterval, workHourStart, workHourEnd);
        this.eventCount = eventCount;
    }

    public Job(int userId, int addressId, String name, String description, int timeInterval, int workHourStart, int workHourEnd, int eventCount, int amount) {
        this(userId, addressId, name, description, timeInterval, workHourStart, workHourEnd, eventCount);
        this.amount = amount;
    }

    public Job(int id, int userId, int addressId, String name, String description) {
        this(userId, addressId, name, description);
        this.id = id;
    }

    public Job(int id, int userId, int addressId, String name, String description, int timeInterval) {
        this(id, userId, addressId, name, description);
        this.timeInterval = timeInterval;
    }

    public Job(int id, int userId, int addressId, String name, String description, int timeInterval, int workHourStart, int workHourEnd) {
        this(id, userId, addressId, name, description, timeInterval);
        this.workHourStart = workHourStart;
        this.workHourEnd = workHourEnd;
    }

    public Job(int id, int userId, int addressId, String name, String description, int timeInterval, int workHourStart, int workHourEnd, int eventCount) {
        this(id, userId, addressId, name, description, timeInterval, workHourStart, workHourEnd);
        this.eventCount = eventCount;
    }

    public Job(int id, int userId, int addressId, String name, String description, int timeInterval, int workHourStart, int workHourEnd, int eventCount, int amount) {
        this(id, userId, addressId, name, description, timeInterval, workHourStart, workHourEnd, eventCount);
        this.amount = amount;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getAddressId() {
        return this.addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTimeInterval() {
        return this.timeInterval;
    }

    public void setTimeInterval(int timeInterval) {
        this.timeInterval = timeInterval;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Address getAddress() {
        return this.address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getEventCount() {
        return this.eventCount;
    }

    public void setEventCount(int eventCount) {
        this.eventCount = eventCount;
    }

    public int getWorkHourStart() {
        return this.workHourStart;
    }

    public void setWorkHourStart(int workHourStart) {
        this.workHourStart = workHourStart;
    }

    public int getWorkHourEnd() {
        return this.workHourEnd;
    }

    public void setWorkHourEnd(int workHourEnd) {
        this.workHourEnd = workHourEnd;
    }

    public String toString() {
        return "["+
                "id="+getId() + ", " +
                "user_id="+getUserId() + ", " +
                "address_id="+getAddressId() + ", " +
                "name='"+getName() + "', " +
                "amount='"+getAmount() + "', " +
                "description='"+getDescription() + "', " +
                "time_interval="+getTimeInterval() + ", " +
                "event_count="+getEventCount()+ ", "+
                "work_hour_start="+getWorkHourStart()+ ", "+
                "work_hour_end="+getWorkHourEnd()+
                "]";
    }
}
