package app.shedule;

import app.dao.EventDAO;
import app.dao.impl.EventDAOImpl;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.Date;

public class EventChangeStatusJob implements Job {

    private final static Log LOGGER = LogFactory.getLog(EventChangeStatusJob.class);

    ApplicationContext context;
    EventDAO eventDAO;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        context = new ClassPathXmlApplicationContext("Spring-Module.xml");
        eventDAO = (EventDAOImpl) context.getBean("eventDAO");

        LOGGER.info("Выполнен запрос на изменение статуса ивента у неподтвержденных записей");
        eventDAO.changeStatusToExpiredIfTimeLeft();
        LOGGER.info("Завершен в " + new Date().toLocaleString());


    }

}
