package app.util.logger;

import app.util.logger.dao.LoggerDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.sql.DataSource;

public class Logger {

    private final static Log LOGGER = LogFactory.getLog(Logger.class);

    public final static String TYPE_REGISTRATION = "registration";
    public final static String TYPE_CREATE = "create";
    public final static String TYPE_UPDATE = "update";
    public final static String TYPE_DELETE = "delete";

    private ApplicationContext context;
    private LoggerDAO loggerDAO;

    public Logger() {
        context = new ClassPathXmlApplicationContext("Spring-Module.xml");
        loggerDAO = (LoggerDAO) context.getBean("loggerDAO");
    }

    private static DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }



    public void write(String type, int user_id, String user_phone, String user_name, int job_id, int event_id, int address_id, String description) {
//        LOGGER.error(""+type+" "+ user_id+" "+user_phone+" "+user_name+" "+ job_id+" "+ event_id+" "+ address_id+" "+description);
        loggerDAO.write(type, user_id, user_phone, user_name, job_id, event_id, address_id, description);
    }

}

