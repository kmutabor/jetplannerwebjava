package app.dao;


import app.model.Address;
import app.model.Job;

public interface AddressDAO {
    public int insert(Address address);

    public Address findById(int id);

    public int update(Address address);

    public Address delete(int addressId, int userId, String token);
}
