package app.dao;

import app.model.User;

import java.util.List;

public interface UserDAO {
    public int insert(User user);

    public User findByUserPhone(String phone);

    public User findById(int id);

    public User delete(int userId, String token);

}
