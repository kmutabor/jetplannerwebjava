package app.controller;

import app.dao.AddressDAO;
import app.dao.JobDAO;
import app.dao.impl.AddressDAOImpl;
import app.dao.impl.JobDAOImpl;
import app.model.Address;
import app.model.Job;
import app.util.AuthHelper;
import app.util.Constants;
import app.util.ResponseHelper;
import app.util.logger.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class JobController {

    private final static Log LOGGER = LogFactory.getLog(JobController.class);

    private Logger logger = new Logger();

    private ApplicationContext context;
    private JobDAO jobDAO;
    private AddressDAO addressDAO;
    private AuthHelper authHelper;

    JobController(){
        context = new ClassPathXmlApplicationContext("Spring-Module.xml");
        jobDAO = (JobDAOImpl) context.getBean("jobDAO");
        addressDAO = (AddressDAOImpl) context.getBean("addressDAO");
        authHelper = new AuthHelper();
    }

    @RequestMapping(value = Constants.Request.JOB, method = RequestMethod.POST)
    public Job create(
            @RequestParam(value = "user_id", required = true) int userId,
            @RequestParam(value = "token", required = true) String token,
            @RequestParam(value = "name", required = true) String name,
            @RequestParam(value = "amount", required = true) int amount,
            @RequestParam(value = "description", required = true) String description,
            @RequestParam(value = "time_interval", required = true, defaultValue = "60") int timeInterval,
            @RequestParam(value = "work_hour_start", required = true, defaultValue = "8") int workHourStart,
            @RequestParam(value = "work_hour_end", required = true, defaultValue = "16") int workHourEnd,
            @RequestParam(value = "address_id", required = true) int addressId
    ) {

        if (!authHelper.checkAuth(userId, token)) {
            LOGGER.error("Auth error: userId = "+userId+", token = "+ token);
            return ResponseHelper.getEmptyJob();
        }

        Job job = new Job(userId, addressId, name, description, timeInterval, workHourStart, workHourEnd, 0, amount);
        int jobId = jobDAO.insert(job);

        job.setId(jobId);

        LOGGER.info("Insert new job: "+job);

        job = jobDAO.findById(jobId);
        logger.write(Logger.TYPE_CREATE,userId,job.getUser().getPhone(),job.getUser().getName(),job.getId(),0,addressId,"Добавлен job '"+job.getName()+ "' "+job.toString());

        return job;

    }

    @RequestMapping(value = Constants.Request.JOB, method = RequestMethod.GET, params = {"!address_id", "!name", "!description", "user_id", "token"})
    public List<Job> getJobs(
            @RequestParam(value = "user_id", required = true) int userId,
            @RequestParam(value = "token", required = true) String token
    ) {

        if (!authHelper.checkAuth(userId, token)) {
            return ResponseHelper.getEmptyJobList();
        }

        List<Job> jobs = jobDAO.findByUserId(userId);

        if (null == jobs || jobs.size() == 0) {
            return ResponseHelper.getEmptyJobList();
        }

        return jobs;

    }

    @RequestMapping(value = Constants.Request.JOB, method = RequestMethod.GET, params = {"!address_id", "!description", "user_id", "token", "name"})
    public List<Job> getJobsByName(
            @RequestParam(value = "user_id", required = true) int userId,
            @RequestParam(value = "token", required = true) String token,
            @RequestParam(value = "name", required = true) String name) {
        if (!authHelper.checkAuth(userId, token)) {
            return ResponseHelper.getEmptyJobList();
        }

        List<Job> jobs = jobDAO.findByName(name);

        if (null == jobs || jobs.size() == 0) {
            return ResponseHelper.getEmptyJobList();
        }

        return jobs;

    }


    @RequestMapping(value = Constants.Request.JOB, method = RequestMethod.GET, params = {"!address_id", "!name", "user_id", "token", "description"})
    public List<Job> getJobsByDescription(
            @RequestParam(value = "user_id", required = true) int userId,
            @RequestParam(value = "token", required = true) String token,
            @RequestParam(value = "description", required = true) String description
    ) {
        if (!authHelper.checkAuth(userId, token)) {
            return ResponseHelper.getEmptyJobList();
        }

        List<Job> jobs = jobDAO.findByDescription(description);

        if (null == jobs || jobs.size() == 0) {
            return ResponseHelper.getEmptyJobList();
        }

        return jobs;

    }

    @RequestMapping(value = Constants.Request.JOB, method = RequestMethod.GET, params = {"!address_id", "!name", "user_id", "token", "!description", "!name", "job_id"})
    public Job getJobById(
            @RequestParam(value = "user_id", required = true) int userId,
            @RequestParam(value = "token", required = true) String token,
            @RequestParam(value = "job_id", required = true) int id
    ) {
        if (!authHelper.checkAuth(userId, token)) {
            return ResponseHelper.getEmptyJob();
        }

        Job job = jobDAO.findById(id);

        return job;

    }

    @RequestMapping(value = Constants.Request.JOB, method = RequestMethod.PUT,params = {"user_id", "token", "job_id", "name", "description"})
    public Job update(
            @RequestParam(value = "user_id", required = true) int userId,
            @RequestParam(value = "token", required = true) String token,
            @RequestParam(value = "job_id", required = true) int jobId,
            @RequestParam(value = "name", required = true) String name,
            @RequestParam(value = "amount", required = true) int amount,
            @RequestParam(value = "description", required = true) String description,
            @RequestParam(value = "time_interval", required = true, defaultValue = "60") int timeInterval,
            @RequestParam(value = "work_hour_start", required = true, defaultValue = "8") int workHourStart,
            @RequestParam(value = "work_hour_end", required = true, defaultValue = "16") int workHourEnd
    ){
        if (!authHelper.checkAuth(userId, token)) {
            return ResponseHelper.getEmptyJob();
        }

        Job job = jobDAO.findById(jobId);
        job.setName(name);
        job.setAmount(amount);
        job.setDescription(description);
        job.setTimeInterval(timeInterval);
        job.setWorkHourStart(workHourStart);
        job.setWorkHourEnd(workHourEnd);

        int updated = jobDAO.update(token, job);
        if(1 == updated){
            LOGGER.info("Update job to: "+job);
            logger.write(Logger.TYPE_UPDATE, userId, job.getUser().getPhone(), job.getUser().getName(), job.getId(),0,0, "Изменен job '"+job.getName()+ "' "+job.toString());
            return job;
        }
        LOGGER.error("Updated job was rejected! Saved old data.");
        return ResponseHelper.getEmptyJob();
    }

    @RequestMapping(value = Constants.Request.JOB + "/{job_id}/{user_id}/{token}", method = RequestMethod.DELETE)
    public Job delete(
            @PathVariable(value = "job_id", required = true) int jobId,
            @PathVariable(value = "user_id", required = true) int userId,
            @PathVariable(value = "token", required = true) String token
    ) {

        if (!authHelper.checkAuth(userId, token)) {
            return ResponseHelper.getEmptyJob();
        }

        Job job = jobDAO.findById(jobId);
        int addressId = 0;
        if(null != job){
            addressId = job.getAddressId();
            logger.write(Logger.TYPE_DELETE, userId, job.getUser().getPhone(), job.getUser().getName(), job.getId(),0,0, "Удален job '"+job.getName()+ "' "+job.toString());
        }


        job = jobDAO.delete(jobId, userId, token);
        if(addressId > 0){
            addressDAO.delete(addressId, userId, token);
        }

        return job;

    }


}
