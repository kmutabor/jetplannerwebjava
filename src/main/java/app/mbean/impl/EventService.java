package app.mbean.impl;

import app.dao.EventDAO;
import app.mbean.EventServiceMXBean;
import app.util.logger.Logger;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class EventService implements EventServiceMXBean {
    private Logger logger = new Logger();

    private List<String> stringList;
    private EventDAO eventDAO;

    public EventService(EventDAO eventDAO) {
        this.eventDAO = eventDAO;
    }

    @Override
    public List<String> getExpiredEventList() {
        return eventDAO.getExpiredList();
    }

    @Override
    public int getExpiredCount() {
        stringList = eventDAO.getExpiredList();
        return stringList !=null? stringList.size():0;
    }

    @Override
    public void removeExpired() {
        logger.write(Logger.TYPE_DELETE, 0, "+0000000000","ADMIN",0,0,0, new Date().toString()+" Удалены события, срок которых прошел: "+ Arrays.toString(stringList.toArray()));
        eventDAO.deleteExpired();
    }
}
