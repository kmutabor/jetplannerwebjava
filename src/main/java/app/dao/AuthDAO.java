package app.dao;

public interface AuthDAO {
    public boolean auth(int userId, String token);
}
