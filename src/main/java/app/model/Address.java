package app.model;

public class Address {

    private int id;
    private String address;
    private double latitude;
    private double longitude;


    public Address(String address, double latitude, double longitude) {
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Address(int id, String address, double latitude, double longitude) {
        this(address, latitude, longitude);
        this.id = id;
    }


    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    @Override
    public String toString() {
        return "[" +
                "id="+getId()+", "+
                "latitude="+getLatitude()+", "+
                "longitude="+getLongitude()+", "+
                "address='"+getAddress()+"', "+
                "]";
    }
}
