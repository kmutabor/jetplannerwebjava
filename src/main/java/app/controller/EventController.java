package app.controller;

import app.dao.EventDAO;
import app.dao.impl.EventDAOImpl;
import app.model.Event;
import app.util.AuthHelper;
import app.util.Constants;
import app.util.ResponseHelper;
import app.util.logger.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;


@RestController
public class EventController {

    private final static Log LOGGER = LogFactory.getLog(EventController.class);

    private Logger logger = new Logger();

    private ApplicationContext context;
    private EventDAO eventDAO;
    private AuthHelper authHelper;

    EventController(){
        context = new ClassPathXmlApplicationContext("Spring-Module.xml");
        eventDAO = (EventDAOImpl) context.getBean("eventDAO");
        authHelper = new AuthHelper();
    }

    @RequestMapping(value = Constants.Request.EVENT, method = RequestMethod.POST)
    public Event create(
            @RequestParam(value = "user_id", required = true) int userId,
            @RequestParam(value = "token", required = true) String token,
            @RequestParam(value = "job_id", required = true) int jobId,
            @RequestParam(value = "status", required = true) String status,
            @RequestParam(value = "desired_time", required = true) String desiredTime
    ) throws ParseException {

        if (!authHelper.checkAuth(userId, token)) {
            LOGGER.error("Auth error: user_id = "+userId+", token = " + token);
            return ResponseHelper.getEmptyEvent();
        }

        Event event = new Event(userId, jobId, status, desiredTime);
        event = eventDAO.insert(event);
        logger.write(Logger.TYPE_CREATE, userId, "", "", event.getJobId(), event.getId(),0, "Добавлен event '"+event.getId()+" : "+event.getDesiredTime()+"' "+ event.toString());

        return event;

    }


    @RequestMapping(value = Constants.Request.EVENT, method = RequestMethod.GET, params = {"user_id", "token", "!job_id"})
    public List<Event> getEventsByUserId(
            @RequestParam(value = "user_id", required = true) int userId,
            @RequestParam(value = "token", required = true) String token
    ) {

        if (!authHelper.checkAuth(userId, token)) {
            LOGGER.error("Auth error: user_id = "+userId+", token = " + token);
            return ResponseHelper.getEmptyEventList();
        }

        List<Event> events = eventDAO.findByUserId(userId);

        if (null == events || events.size() == 0) {
            return ResponseHelper.getEmptyEventList();
        }

        return events;

    }


    @RequestMapping(value = Constants.Request.EVENT, method = RequestMethod.GET, params = {"user_id", "token", "job_id", "!client"})
    public List<Event> getEventsByJobId(
            @RequestParam(value = "user_id", required = true) int userId,
            @RequestParam(value = "token", required = true) String token,
            @RequestParam(value = "job_id", required = true) int jobId
    ) {

        if (!authHelper.checkAuth(userId, token)) {
            LOGGER.error("Auth error: user_id = "+userId+", token = " + token);
            return ResponseHelper.getEmptyEventList();
        }

        List<Event> events = eventDAO.findByJobId(jobId);

        if (null == events || events.size() == 0) {
            return ResponseHelper.getEmptyEventList();
        }

        return events;
    }

    @RequestMapping(value = Constants.Request.EVENT, method = RequestMethod.GET, params = {"user_id", "token", "job_id", "client"})
    public List<Event> getEventsByJobIdForClient(
            @RequestParam(value = "user_id", required = true) int userId,
            @RequestParam(value = "token", required = true) String token,
            @RequestParam(value = "job_id", required = true) int jobId,
            @RequestParam(value = "client", required = true) boolean client
    ) {

        if (!authHelper.checkAuth(userId, token)) {
            LOGGER.error("Auth error: user_id = "+userId+", token = " + token);
            return ResponseHelper.getEmptyEventList();
        }

        List<Event> events = eventDAO.findByJobIdForClientRequest(jobId);

        if (null == events || events.size() == 0) {
            return ResponseHelper.getEmptyEventList();
        }

        return events;
    }

    @RequestMapping(value = Constants.Request.EVENT, method = RequestMethod.PUT,params = {"user_id", "token", "job_id", "event_id", "event_status"})
    public Event updateStatus(
            @RequestParam(value = "user_id", required = true) int userId,
            @RequestParam(value = "token", required = true) String token,
            @RequestParam(value = "job_id", required = true) int jobId,
            @RequestParam(value = "event_id", required = true) int eventId,
            @RequestParam(value = "event_status", required = true) String status
    ){

        if (!authHelper.checkAuth(userId, token)) {
            LOGGER.error("Auth error: user_id = "+userId+", token = " + token);
            return ResponseHelper.getEmptyEvent();
        }

        int updated = eventDAO.updateStatus(userId, token, jobId, eventId, status);
        if(1 == updated){
            LOGGER.info("Update event '" + eventId + "' for job '" + jobId + "' status to: " + status);
            Event event = eventDAO.findById(eventId);
            logger.write(Logger.TYPE_UPDATE, userId, event.getUser().getPhone(), event.getUser().getName(), event.getJobId(), event.getId(), 0, "Обновлен статус event '"+event.getId()+" : "+event.getDesiredTime()+"' "+ event.toString());
            return event;
        }
        LOGGER.error("Updated event was rejected! Saved old data.");
        return ResponseHelper.getEmptyEvent();
    }


    @RequestMapping(value = Constants.Request.EVENT + "/{event_id}/{user_id}/{token}", method = RequestMethod.DELETE)
    public Event delete(
            @PathVariable(value = "event_id", required = true) int eventId,
            @PathVariable(value = "user_id", required = true) int userId,
            @PathVariable(value = "token", required = true) String token
    ) {

        if (!authHelper.checkAuth(userId, token)) {
            LOGGER.error("Auth error: user_id = "+userId+", token = " + token);
            return ResponseHelper.getEmptyEvent();
        }

        LOGGER.info("Deleted event id: "+eventId);

        Event event = eventDAO.findById(eventId);
        if(0 < event.getId()){
            logger.write(Logger.TYPE_DELETE, userId, event.getUser().getPhone(), event.getUser().getName(), event.getJobId(),event.getId(),0, "Удален event '"+event.getId()+" : "+event.getDesiredTime()+"' "+ event.toString());
        }


        return eventDAO.delete(eventId, userId, token);

    }


}
