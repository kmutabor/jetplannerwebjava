package app.dao.impl;

import app.dao.JobDAO;
import app.model.Address;
import app.model.Job;
import app.model.User;
import app.util.ResponseHelper;
import com.mysql.jdbc.Statement;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JobDAOImpl implements JobDAO {


    private final static Log LOGGER = LogFactory.getLog(JobDAOImpl.class);

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    /**
     * @param job
     * @return
     */
    @Override
    public int insert(Job job) {
        String sql = "INSERT INTO " +
                "job (user_id, address_id, name, description, time_interval, work_hour_start, work_hour_end, amount) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = null;
        PreparedStatement ps = null;
        int result = 0;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false); // begin transaction;

            ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, job.getUserId());
            ps.setInt(2, job.getAddressId());
            ps.setString(3, job.getName());
            ps.setString(4, job.getDescription());
            ps.setInt(5, job.getTimeInterval());
            ps.setInt(6, job.getWorkHourStart());
            ps.setInt(7, job.getWorkHourEnd());
            ps.setInt(8, job.getAmount());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                result = rs.getInt(1); // get last inserted id
            }
            ps.close();

            conn.commit(); // commit
            conn.setAutoCommit(true);

        } catch (SQLException e) {
            e.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException e1) {
                e.printStackTrace();
                result = 0;
            }
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                result = 0;
            }
        }
        return result;
    }

    @Override
    public List<Job> findByUserId(int userId) {
        String sql = "SELECT *, (SELECT COUNT(e.id) FROM event AS e WHERE e.job_id = j.id) AS event_count " +
                "FROM job AS j " +
                "JOIN user AS u ON u.id = j.user_id " +
                "JOIN address AS a ON a.id = j.address_id " +
                "WHERE j.user_id = ? "
//                "ORDER BY event_count DESC "
                ;
        Connection conn = null;
        Job job = null;
        List<Job> jobs = new ArrayList<Job>();

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, userId);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                job = new Job(
                        rs.getInt("j.id"),
                        rs.getInt("j.user_id"),
                        rs.getInt("j.address_id"),
                        rs.getString("j.name"),
                        rs.getString("j.description"),
                        rs.getInt("j.time_interval"),
                        rs.getInt("j.work_hour_start"),
                        rs.getInt("j.work_hour_end"),
                        rs.getInt("event_count"),
                        rs.getInt("j.amount")
                );
                job.setUser(new User(rs.getInt("u.id"), "", rs.getString("u.phone"), rs.getString("u.name")));
                job.setAddress(new Address(rs.getInt("a.id"), rs.getString("a.address"),
                        rs.getDouble("a.latitude"), rs.getDouble("a.longitude")));

                jobs.add(job);
            }
            rs.close();
            ps.close();
            if (jobs.isEmpty()) {
                jobs = ResponseHelper.getEmptyJobList();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            jobs = ResponseHelper.getEmptyJobList();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    jobs = ResponseHelper.getEmptyJobList();
                }
            }
        }
        return jobs;
    }

    @Override
    public List<Job> findByName(String name) {
        String sql = "SELECT * FROM job AS j " +
                "JOIN user AS u ON u.id = j.user_id " +
                "JOIN address AS a ON a.id = j.address_id " +
                "WHERE j.name = ?";
        Connection conn = null;
        Job job = null;
        List<Job> jobs = new ArrayList<Job>();

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, name);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                job = fillJobData(rs);
                jobs.add(job);
            }
            rs.close();
            ps.close();
            if (jobs.isEmpty()) {
                jobs = ResponseHelper.getEmptyJobList();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            jobs = ResponseHelper.getEmptyJobList();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    jobs = ResponseHelper.getEmptyJobList();
                }
            }
        }
        return jobs;
    }

    @Override
    public List<Job> findByDescription(String description) {
        String sql = "SELECT * FROM job AS j " +
                "JOIN user AS u ON u.id = j.user_id " +
                "JOIN address AS a ON a.id = j.address_id " +
                "WHERE description LIKE ? ESCAPE '!'";
        Connection conn = null;
        Job job = null;
        List<Job> jobs = new ArrayList<Job>();

        description = description
                .replace("!", "!!")
                .replace("%", "!%")
                .replace("_", "!_")
                .replace("[", "![");

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + description + "%");

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                job = fillJobData(rs);
                jobs.add(job);
            }
            rs.close();
            ps.close();
            if (jobs.isEmpty()) {
                jobs = ResponseHelper.getEmptyJobList();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            jobs = ResponseHelper.getEmptyJobList();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    jobs = ResponseHelper.getEmptyJobList();
                }
            }
        }
        return jobs;
    }


    @Override
    public Job findById(int id) {

        String sql = "SELECT * FROM job AS j " +
                "JOIN user AS u ON u.id = j.user_id " +
                "JOIN address AS a ON a.id = j.address_id " +
                "WHERE j.id = ? ";
        Connection conn = null;
        Job job = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id );

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                job = fillJobData(rs);
            }else{
                job = ResponseHelper.getEmptyJob();
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
            job = ResponseHelper.getEmptyJob();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    job = ResponseHelper.getEmptyJob();
                }
            }
        }
        return job;
    }




    @Override
    public Job delete(int jobId, int userId, String token) {
        String sql = "DELETE j FROM job AS j " +
                "JOIN user AS u ON u.id = j.user_id " +
                "WHERE j.id = ? AND j.user_id = ? AND u.token = ?";
        Connection conn = null;
        Job response = null;
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, jobId);
            ps.setInt(2, userId);
            ps.setString(3, token);
            ps.executeUpdate();
            ps.close();
            response = new Job(jobId, userId, 0, "deleted", "deleted");

        } catch (SQLException e) {
            e.printStackTrace();
            response = ResponseHelper.getEmptyJob();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    response = ResponseHelper.getEmptyJob();
                }
            }
        }
        return response;
    }

    public int update(String token, Job job){
        String sql = "UPDATE job AS j " +
                "SET j.name = ?, j.amount = ?, j.description = ?, j.time_interval = ?, j.work_hour_start = ?, j.work_hour_end = ? " +
                "WHERE j.id = ? AND j.user_id = ? AND (SELECT u.token FROM user AS u where u.id = j.user_id) = ?";
        Connection conn = null;
        PreparedStatement ps = null;
        int result = 0;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false); // begin transaction;

            ps = conn.prepareStatement(sql);
            ps.setString(1, job.getName());
            ps.setInt(2, job.getAmount());
            ps.setString(3, job.getDescription());
            ps.setInt(4, job.getTimeInterval());
            ps.setInt(5, job.getWorkHourStart());
            ps.setInt(6, job.getWorkHourEnd());
            ps.setInt(7, job.getId());
            ps.setInt(8, job.getUserId());
            ps.setString(9, token);
            result = ps.executeUpdate();
            ps.close();

            conn.commit(); // commit
            conn.setAutoCommit(true);

        } catch (SQLException e) {
            e.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException e1) {
                e.printStackTrace();
                result = 0;
            }
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                result = 0;
            }
        }
        return result;
    }




    /**
     * Заполняет обхект job данными
     * @param rs
     * @return
     * @throws SQLException
     */
    private Job fillJobData(ResultSet rs) throws SQLException {
        Job job = new Job(
                rs.getInt("j.id"),
                rs.getInt("j.user_id"),
                rs.getInt("j.address_id"),
                rs.getString("j.name"),
                rs.getString("j.description"),
                rs.getInt("j.time_interval"),
                rs.getInt("j.work_hour_start"),
                rs.getInt("j.work_hour_end"),
                0,
                rs.getInt("j.amount")

        );
        job.setUser(new User(rs.getInt("u.id"), "", rs.getString("u.phone"), rs.getString("u.name")));
        job.setAddress(new Address(rs.getInt("a.id"), rs.getString("a.address"), rs.getDouble("a.latitude"), rs.getDouble("a.longitude")));
        return job;
    }

}
