package app.dao.impl;

import app.dao.EventDAO;
import app.model.Event;
import app.model.Job;
import app.model.User;
import app.util.ResponseHelper;
import com.mysql.jdbc.Statement;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EventDAOImpl implements EventDAO {

    private final static Log LOGGER = LogFactory.getLog(EventDAOImpl.class);

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    /**
     * @param event
     * @return
     */
    @Override
    public Event insert(Event event) {
        String sql = "INSERT INTO event (user_id, job_id, status, desired_time) VALUES (?, ?, ?, ?)";
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false); // begin transaction;

            ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, event.getUserId());
            ps.setInt(2, event.getJobId());
            ps.setString(3, event.getStatus());
            ps.setString(4, event.getDesiredTime());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            int newId = 0;
            if (rs.next()) {
                newId = rs.getInt(1); // get last inserted id
            }
            ps.close();
            conn.commit(); // commit

            event.setId(newId);

        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println(e.getErrorCode());
            if(e.getErrorCode() == 1062){
                event.setId(0);
                event.setDesiredTime("busy");
            }else {
                try {
                    conn.rollback();
                } catch (SQLException e1) {
                    e.printStackTrace();
                }
                event = ResponseHelper.getEmptyEvent();
            }
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                }
            } catch (SQLException e) {
                event = ResponseHelper.getEmptyEvent();
            }
        }
        return event;
    }


    @Override
    public Event findById(int id) {
        String sql = "SELECT * FROM event AS e " +
                "JOIN job AS j ON j.id = e.job_id "+
                "JOIN user AS u ON u.id = e.user_id "+
                "WHERE e.id = ?";
        Connection conn = null;
        Event event = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                event = new Event(
                        rs.getInt("e.id"),
                        rs.getInt("e.user_id"),
                        rs.getInt("e.job_id"),
                        rs.getString("e.status"),
                        rs.getString("e.desired_time")
                );
                event.setJob(new Job(rs.getInt("j.id") ,rs.getInt("j.user_id"), rs.getInt("j.address_id"), rs.getString("j.name"), rs.getString("j.description")));
                event.setUser(new User(rs.getInt("u.id"), "", rs.getString("u.phone"), rs.getString("u.name")));
            }else{
                event = ResponseHelper.getEmptyEvent();
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    event = ResponseHelper.getEmptyEvent();
                }
            }
        }
        return event;
    }

    /**
     * @param userId
     * @return
     */
    @Override
    public List<Event> findByUserId(int userId) {
        String sql = "SELECT * FROM event AS e " +
                "JOIN job AS j ON j.id = e.job_id "+
                "JOIN user AS u ON u.id = j.user_id "+
                "WHERE e.user_id = ?";
//        "AND (e.status = 'request' OR e.status = 'approved') ";

        return this.doGetRequest(sql, userId);
    }

    /**
     * @param jobId
     * @return
     */
    @Override
    public List<Event> findByJobId(int jobId) {
        String sql = "SELECT * FROM event AS e " +
                "JOIN job AS j ON j.id = e.job_id "+
                "JOIN user AS u ON u.id = e.user_id "+
                "WHERE e.job_id = ?";
        return this.doGetRequest(sql, jobId);
    }


    /**
     * @param jobId
     * @return
     */
    @Override
    public List<Event> findByJobIdForClientRequest(int jobId) {
        String sql = "SELECT * FROM event AS e " +
                "JOIN job AS j ON j.id = e.job_id "+
                "JOIN user AS u ON u.id = e.user_id "+
                "WHERE e.job_id = ? " +
                "AND (e.status = 'request' OR e.status = 'approved') " +
                "AND e.desired_time BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 28 DAY);";
        return this.doGetRequest(sql, jobId);
    }

    /**
     *
     * @param userId
     * @param token
     * @param jobId
     * @param eventId
     * @param status
     * @return
     */
    @Override
    public int updateStatus(int userId, String token, int jobId, int eventId, String status){
        String sql = "UPDATE event AS e SET e.status = ? " +
                "WHERE e.id = ? " +
                "AND e.job_id = ? "+
                "AND ((SELECT u.token FROM user AS u where u.id = ? ) = ? " +
                "OR (SELECT u.token FROM user AS u, job AS j where u.id = ? AND j.user_id = u.id) = ? ) ";
        Connection conn = null;
        PreparedStatement ps = null;
        int result = 0;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false); // begin transaction;

            ps = conn.prepareStatement(sql);
            ps.setString(1, status);
            ps.setInt(2, eventId);
            ps.setInt(3, jobId);
            ps.setInt(4, userId);
            ps.setString(5, token);
            ps.setInt(6, userId);
            ps.setString(7, token);

            result = ps.executeUpdate();
            ps.close();

            conn.commit(); // commit
            conn.setAutoCommit(true);

        } catch (SQLException e) {
            e.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException e1) {
                result = 0;
            }
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                result = 0;
            }
        }
        return result;
    }


    @Override
    public Event delete(int eventId, int userId, String token) {
        String sql = "DELETE e FROM event AS e " +
                "JOIN user AS u ON u.id = e.user_id " +
                "WHERE e.id = ? AND e.user_id = ? AND u.token = ?";
        Connection conn = null;
        Event response = null;
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, eventId);
            ps.setInt(2, userId);
            ps.setString(3, token);
            ps.executeUpdate();
            ps.close();
            response = new Event(eventId, 0, 0, "deleted", "deleted");
        } catch (SQLException e) {
            e.printStackTrace();
            response = ResponseHelper.getEmptyEvent();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    response = ResponseHelper.getEmptyEvent();
                }
            }
        }
        return response;
    }


    private List<Event> doGetRequest(String sql, int param) {
        Connection conn = null;
        Event event = null;
        List<Event> events = new ArrayList<Event>();

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, param);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                event = new Event(
                        rs.getInt("e.id"),
                        rs.getInt("e.user_id"),
                        rs.getInt("e.job_id"),
                        rs.getString("e.status"),
                        rs.getString("e.desired_time")
                );
                event.setJob(new Job(rs.getInt("j.id") ,rs.getInt("j.user_id"), rs.getInt("j.address_id"), rs.getString("j.name"), rs.getString("j.description")));
                event.setUser(new User(rs.getInt("u.id"), "", rs.getString("u.phone"), rs.getString("u.name")));
                events.add(event);
            }
            rs.close();
            ps.close();
            if (events.isEmpty()) {
                events = ResponseHelper.getEmptyEventList();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    events = ResponseHelper.getEmptyEventList();
                }
            }
        }
        return events;
    }


    @Override
    public void deleteExpired() {
        String sql = "DELETE e FROM event AS e WHERE e.status = 'expired'; ";
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            conn.createStatement().execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public List<String> getExpiredList() {
        String sql = "SELECT e.id, e.desired_time, u.id, u.name, u.phone, j.id, j.name, j.description FROM event AS e " +
                "JOIN job AS j ON j.id = e.job_id " +
                "JOIN user AS u ON u.id = e.user_id " +
                "WHERE e.status = 'expired'; ";
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            ResultSet resultSet = conn.createStatement().executeQuery(sql);
            List<String> integers = new ArrayList<>();
            while (resultSet.next()){
                int event_id = resultSet.getInt("e.id");
                String desired_time = resultSet.getString("e.desired_time");
                int user_id = resultSet.getInt("u.id");
                String user_name = resultSet.getString("u.name");
                String user_phone = resultSet.getString("u.phone");
                int job_id = resultSet.getInt("j.id");
                String job_name = resultSet.getString("j.name");
                String job_description = resultSet.getString("j.description");
                String message = "[" +
                        "event_id="+ event_id + ", "+
                        "event_desired_time="+ desired_time + ", "+
                        "user_id="+ user_id + ", "+
                        "user_name="+ user_name + ", "+
                        "user_phone="+ user_phone + ", "+
                        "job_id="+ job_id + ", "+
                        "job_name="+ job_name + ", "+
                        "job_description="+ job_description +
                        "]";
                integers.add(message);
            }
            return integers;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }


    public void changeStatusToExpiredIfTimeLeft(){
        String sql = "UPDATE event AS e SET e.status = 'expired' " +
                "WHERE e.status = 'request' AND e.desired_time < NOW()";
        Connection conn = null;
        int result = 0;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false); // begin transaction;

            conn.createStatement().executeUpdate(sql);

            conn.commit(); // commit
            conn.setAutoCommit(true);

        } catch (SQLException e) {
            e.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException e1) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

}


