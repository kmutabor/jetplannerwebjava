package app.dao.impl;

import app.dao.AddressDAO;
import app.model.Address;
import app.model.Job;
import app.util.ResponseHelper;
import com.mysql.jdbc.Statement;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AddressDAOImpl implements AddressDAO {

    private final static Log LOGGER = LogFactory.getLog(AddressDAOImpl.class);

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    /**
     * @param address
     * @return inserted id or 0
     */
    @Override
    public int insert(Address address) {
        String sql = "INSERT INTO address (address, latitude, longitude) VALUES (?, ?, ?)";
        Connection conn = null;
        PreparedStatement ps = null;
        int result = 0;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false); // begin transaction;

            ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, address.getAddress());
            ps.setDouble(2, address.getLatitude());
            ps.setDouble(3, address.getLongitude());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                result = rs.getInt(1); // get last inserted id
            }
            ps.close();
            conn.commit(); // commit

            LOGGER.info("New Address id : "+ result);

        } catch (SQLException e) {
            e.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException e1) {
                e.printStackTrace();
                result = 0;
            }
        } finally {
            try {
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                }
            } catch (SQLException e) {
                result = 0;
                LOGGER.error(e.getMessage());
            }
        }
        return result;
    }



    @Override
    public Address delete(int addressId, int userId, String token) {
        String sql = "DELETE a FROM address AS a WHERE a.id = ? " +
                "AND a.id NOT IN(SELECT j.address_id FROM job AS j WHERE j.user_id = ?)";
        Connection conn = null;
        Address response = null;
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, addressId);
            ps.setInt(2, userId);
            ps.executeUpdate();
            ps.close();
            response = new Address(addressId, "deleted", 0.0, 0.0);
        } catch (SQLException e) {
            e.printStackTrace();
            response = ResponseHelper.getEmptyAddress();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
        }
        return response;
    }

    @Override
    public Address findById(int id) {
        String sql = "SELECT * FROM address WHERE id = ?";
        Connection conn = null;
        Address address = null;
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                address = new Address(
                        rs.getInt("id"),
                        rs.getString("address"),
                        rs.getDouble("latitude"),
                        rs.getDouble("longitude")
                );
            } else {
                address = ResponseHelper.getEmptyAddress();
            }
            rs.close();
            ps.close();
            return address;
        } catch (SQLException e) {
            e.printStackTrace();
            address = ResponseHelper.getEmptyAddress();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return address;
    }


    public int update(Address address){
        String sql = "UPDATE address AS a " +
                "SET a.address = ?, a.latitude = ?, a.longitude = ? " +
                "WHERE a.id = ?";
        Connection conn = null;
        PreparedStatement ps = null;
        int result = 0;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false); // begin transaction;

            ps = conn.prepareStatement(sql);
            ps.setString(1, address.getAddress());
            ps.setDouble(2, address.getLatitude());
            ps.setDouble(3, address.getLongitude());
            ps.setInt(4, address.getId());
            result = ps.executeUpdate();
            ps.close();

            conn.commit(); // commit
            conn.setAutoCommit(true);

        } catch (SQLException e) {
            e.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException e1) {
                e.printStackTrace();
                result = 0;
            }
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                result = 0;
            }
        }
        return result;
    }



}
