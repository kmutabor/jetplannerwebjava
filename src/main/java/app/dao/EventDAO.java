package app.dao;

import app.model.Event;

import java.util.List;

public interface EventDAO {
    public Event insert(Event event);

    public Event findById(int userId);

    public List<Event> findByUserId(int userId);

    public List<Event> findByJobId(int jobId);

    public List<Event> findByJobIdForClientRequest(int jobId);

    public int updateStatus(int userId, String token, int jobId, int eventId, String status);

    public Event delete(int jobId, int userId, String token);

    public void deleteExpired();

    public List<String> getExpiredList();

    public void changeStatusToExpiredIfTimeLeft();
}
