package app.dao.impl;

import app.model.User;
import app.util.ResponseHelper;
import com.mysql.jdbc.Statement;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDAOImpl implements app.dao.UserDAO {

    private final static Log LOGGER = LogFactory.getLog(UserDAOImpl.class);

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    /**
     * Добавсить пользователя
     *
     * @param user
     * @return id пользователя или 0
     * @throws SQLException
     */
    @Override
    public int insert(User user) {
        String sql = "INSERT INTO user (phone, token, name) VALUES (?, ?, ?)";
        Connection conn = null;
        PreparedStatement ps = null;
        int result = 0;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false); // begin transaction;

            ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.getPhone());
            ps.setString(2, user.getToken());
            ps.setString(3, user.getName());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                result = rs.getInt(1); // get last inserted id
            }
            ps.close();
            conn.commit(); // commit
            conn.setAutoCommit(true);

            LOGGER.info("User " + result + " added");

        } catch (SQLException e) {
            e.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException e1) {
                e.printStackTrace();
                result = 0;
            }
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                result = 0;
            }
        }
        return result;
    }

    @Override
    public User findById(int id) {
        String sql = "SELECT id, phone, token, name FROM user WHERE id = ?";
        Connection conn = null;
        User user = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = new User(
                        rs.getInt("id"),
                        rs.getString("phone"),
                        rs.getString("token"),
                        rs.getString("name")
                );
            } else {
                user = ResponseHelper.getEmptyUser();
            }
            rs.close();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
            user = ResponseHelper.getEmptyUser();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    user = ResponseHelper.getEmptyUser();
                }
            }
        }
        return user;
    }

    @Override
    public User findByUserPhone(String phone) {
        String sql = "SELECT id, phone, token, name FROM user WHERE phone = ?";
        Connection conn = null;
        User user = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, phone);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = new User(
                        rs.getInt("id"),
                        rs.getString("phone"),
                        rs.getString("token"),
                        rs.getString("name")
                );
            } else {
                user = ResponseHelper.getEmptyUser();
            }
            rs.close();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
            user = ResponseHelper.getEmptyUser();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    user = ResponseHelper.getEmptyUser();
                }
            }
        }
        return user;
    }


    @Override
    public User delete(int userId, String token) {
        String sql = "DELETE FROM user WHERE id = ? AND token = ?";
        Connection conn = null;
        User response = null;
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, userId);
            ps.setString(2, token);
            ps.executeUpdate();
            ps.close();
            response = new User(userId, "deleted", "deleted", "deleted");
        } catch (SQLException e) {
            e.printStackTrace();
            response = ResponseHelper.getEmptyUser();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    response = ResponseHelper.getEmptyUser();
                }
            }
        }
        return response;
    }
}
